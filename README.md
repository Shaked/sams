## Account Management Web Service

Share the following capabilities:

- Create accounts.
- Deactivate accounts.
- Create transactions which:
    - Deposit amounts.
    - Withdraw amounts.
    - Transfer amounts between accounts.


### Installation

```
$ make install
```

in case needed, you can update later with `make update`

### Usage

```
$ make run
```

Use the [API documentation](API.md) for more information.

### Tests

```
$ make test
```

### Coverage

```
$ make cover
```

Note that this command will open your browser in order to see the coverage clearly.

