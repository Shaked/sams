## API

### Endpoints


| Endpoint                            | Method | Parameters | Description |
|-------------------------------------|--------|------------|-------------|
| `/account/create/`                  | POST   | balance(float) |  Creates a new account |
| `/account/{accountId}`              | DELETE | | Deactivates an account |
| `/account/deposit/{accountId}`      | PUT    | *amount(float) | Deposits to account |
| `/account/withdraw/{accountId}`     | PUT    | *amount(float) | Withdraws from account |
| `/account/transfer/{fromAccountId}` | PUT    | *toAccountId(string) <br/> *amount(float) | Transfers from one account to another |

Note that parameters that are marked with an * are **required**.