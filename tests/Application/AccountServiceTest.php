<?php
use BigName\EventDispatcher\Dispatcher;
use BigName\EventDispatcher\Listener;
use Infra\MemoryRepository;
use \Domain\Account\CouldNotDepositAccountException;

class AccountServiceTest extends \PHPUnit_Framework_TestCase {
	public function testThatDepositFailed() {
		$this->setExpectedException(CouldNotDepositAccountException::class);
		$dispatcher = new Dispatcher;
		$saga = new \Domain\Saga($dispatcher);
		$repository = $this->getMock(MemoryRepository::class, ['deposit']);
		$repository
			->expects($this->any())
			->method('deposit')
			->will($this->returnValue(false))
		;
		$service = new \Application\AccountService(
			$dispatcher,
			$repository
		);
		$account = $service->execute(10.24);
	}

	public function testThatUserHasBeenCreated() {
		$mockListener = $this->getMock(Listener::class, ['handle']);
		$mockListener
			->expects($this->any())
			->method('handle')
			->will($this->returnCallback(function ($e) {
				$this->assertEquals($e->getName(), 'AccountDeposited');
				$this->assertEquals($e->getAccountId(), 1);
			}))
		;
		$dispatcher = new Dispatcher;
		$dispatcher
			->addListener('AccountDeposited', $mockListener)
		;

		$saga = new \Domain\Saga($dispatcher);
		$repository = new MemoryRepository();
		$service = new \Application\AccountService(
			$dispatcher,
			$repository
		);
		$account = $service->execute(10.24);
		$accounts = $repository->fetchAccounts();
		$this->assertCount(1, $accounts);
		$actualBalance = $accounts[1];
		$expectedBalance = new \Domain\Balance(10.24);
		$this->assertTrue($actualBalance->equals($expectedBalance));
	}
}