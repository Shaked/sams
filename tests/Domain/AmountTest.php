<?php
use \Domain\Amount;

class AmountTest extends \PHPUnit_Framework_TestCase {
	public function testEquals() {
		$amount1 = new Amount(10.52);
		$amount2 = new Amount(10.52);
		$this->assertTrue($amount1->equals($amount2));
		$amount3 = new Amount(10.54);
		$this->assertFalse($amount1->equals($amount3));
	}
}