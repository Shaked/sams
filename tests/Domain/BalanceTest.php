<?php

class BalanceTest extends \PHPUnit_Framework_TestCase {
	public function testToString() {
		$balance = new \Domain\Balance(10.52);
		$this->assertequals('10.52', (string) $balance);
	}
}