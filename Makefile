install:
	composer self-update && composer install --no-interaction --prefer-source

update:
	composer self-update && composer update --no-interaction --prefer-source

test:
	./bin/phpunit

cover:
	rm -rf ./coverage/
	./bin/phpunit --coverage-html ./coverage/ && open ./coverage/index.html

run:
	php -S localhost:9002 ./web/app.php