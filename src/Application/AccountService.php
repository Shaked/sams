<?php
namespace Application;

use BigName\EventDispatcher\Dispatcher;
use \Domain\Account\Repository;

class AccountService {
	/**
	 * @var Dispatcher
	 */
	private $dispatcher;
	/**
	 * @var Repository
	 */
	private $repository;

	/**
	 * @param Dispatcher $dispatcher
	 * @param Repository $repository
	 */
	public function __construct(
		Dispatcher $dispatcher,
		Repository $repository
	) {
		$this->dispatcher = $dispatcher;
		$this->repository = $repository;
	}

	/**
	 * @param $balance
	 */
	public function execute($balance) {
		$account = new \Domain\AccountAggregate(
			$this->dispatcher,
			$this->repository,
			$this->repository->generateId()
		);

		return $account->create(new \Domain\Balance($balance));
	}
}