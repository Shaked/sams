<?php
namespace Domain;
use BigName\EventDispatcher\Dispatcher;
use \Domain\Account\Handler\DepositAmount;
use \Domain\Account\Repository;

final class AccountAggregate {
	use HasEvents;
	/**
	 * @var string
	 */
	private $id;
	/**
	 * @var Account\Balance
	 */
	private $balance;
	/**
	 * @var Dispatcher
	 */
	private $dispatcher;
	/**
	 * @param $id
	 * @param $balance
	 */
	public function __construct(
		Dispatcher $dispatcher,
		Repository $repository,
		$id
	) {
		$this->dispatcher = $dispatcher;
		$this->repository = $repository;
		$this->id = $id;
		$this->registerDomainEvents($dispatcher);
	}

	/**
	 * @param Account\Balance $balance
	 */
	public function create(Balance $balance) {
		$this->balance = $balance;
		$this->repository->create($this->id);
		$this->record(new Account\Event\AccountCreated($this));
		$this->dispatcher->dispatch($this->release());
		return $this;
	}

	/**
	 * @return \Domain\Balance
	 */
	public function getBalance() {
		return $this->balance;
	}

	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	private function registerDomainEvents() {
		$this->dispatcher->addListener(
			'DepositAmount',
			new DepositAmount($this->dispatcher, $this->repository)
		);
	}

}