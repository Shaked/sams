<?php
namespace Domain;

class Amount {
	/**
	 * @var float
	 */
	private $amount = 0.0;
	/**
	 * @param $amount
	 */
	public function __construct($amount) {
		$this->amount = $amount;
	}

	/**
	 * @todo  see ARCHITECTURE.md
	 * @param Amount $amount
	 * @return mixed
	 */
	public function equals(Amount $amount) {
		return $amount->getAmount() == $this->amount;
	}

	/**
	 * @return mixed
	 */
	public function getAmount() {
		return $this->amount;
	}
}