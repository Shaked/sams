<?php
namespace Domain;

class Balance {
	/**
	 * @var float
	 */
	private $balance = 0.0;

	/**
	 * @param $balance
	 */
	public function __construct($balance) {
		$this->balance = $balance;
	}

	public function __toString() {
		return number_format($this->balance, 2);
	}

	/**
	 * @param Amount $amount
	 */
	public function deposit(Amount $amount) {
		return new self($this->balance + $amount->getAmount());
	}

	/**
	 * @todo  see ARCHITECTURE.md
	 * @param Balance $balance
	 * @return mixed
	 */
	public function equals(Balance $balance) {
		return $balance->getBalance() == $this->balance;
	}

	/**
	 * @return mixed
	 */
	public function getBalance() {
		return $this->balance;
	}
}