<?php
namespace Domain;

use BigName\EventDispatcher\Dispatcher;
use BigName\EventDispatcher\Event;
use BigName\EventDispatcher\Listener;
use \Domain\Account\Event\DepositAmount;

class Saga implements Listener {
	use HasEvents;

	/**
	 * @var mixed
	 */
	private $dispatcher;

	/**
	 * @param Dispatcher $dispatcher
	 */
	public function __construct(
		Dispatcher $dispatcher
	) {
		$this->dispatcher = $dispatcher;
		$this->dispatcher->addListener('AccountCreated', $this);
		$this->dispatcher->addListener('AccountDeposited', $this);
		$this->dispatcher->addListener('DepositAmount', $this);
	}

	/**
	 * @param Event $event
	 */
	public function handle(Event $event) {
		switch ($event->getName()) {
		case 'AccountCreated':
			$balance = $event->getAccount()->getBalance();
			$amount = new Amount($balance->getBalance());
			$accountId = $event->getAccount()->getId();
			$this->dispatcher->dispatch(new DepositAmount($amount, $accountId));
			break;
		case 'DepositAmount':
			//todo add logic
			break;
		case 'AccountDeposited':
			$accountId = $event->getAccountId();
			//todo add logic
			break;
		}
	}
}