<?php
namespace Domain\Account\Event;

use BigName\EventDispatcher\Event;

class AccountCreated implements Event {
	/**
	 * @var \Domain\AccountAggregate
	 */
	private $account;

	/**
	 * @param \Domain\AccountAggregate $account
	 */
	public function __construct(\Domain\AccountAggregate $account) {
		$this->account = $account;
	}

	/**
	 * @return \Domain\AccountAggregate
	 */
	public function getAccount() {
		return $this->account;
	}

	/**
	 * Return the name of the event
	 *
	 * @return string
	 */
	public function getName() {
		return 'AccountCreated';
	}
}