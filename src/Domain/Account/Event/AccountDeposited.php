<?php
namespace Domain\Account\Event;

use BigName\EventDispatcher\Event;

class AccountDeposited implements Event {
	/**
	 * @var mixed
	 */
	private $accountId;

	/**
	 * @param $accountId
	 */
	public function __construct($accountId) {
		$this->accountId = $accountId;
	}

	/**
	 * @return mixed
	 */
	public function getAccountId() {
		return $this->accountId;
	}

	/**
	 * Return the name of the event
	 *
	 * @return string
	 */
	public function getName() {
		return 'AccountDeposited';
	}
}