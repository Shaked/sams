<?php
namespace Domain\Account\Event;

use BigName\EventDispatcher\Event;

class DepositAmount implements Event {
	/**
	 * @var mixed
	 */
	private $accountId;
	/**
	 * @var mixed
	 */
	private $amount;

	/**
	 * @param \Domain\Amount $amount
	 * @param $accountId
	 */
	public function __construct(\Domain\Amount $amount, $accountId) {
		$this->amount = $amount;
		$this->accountId = $accountId;
	}

	/**
	 * @return mixed
	 */
	public function getAccountId() {
		return $this->accountId;
	}

	/**
	 * @return mixed
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 * Return the name of the event
	 *
	 * @return string
	 */
	public function getName() {
		return 'DepositAmount';
	}
}