<?php
namespace Domain\Account;
use \Domain\Amount;

interface Repository {
	/**
	 * @param $id
	 * @param \Domain\Balance $balance
	 */
	public function create($id);

	public function generateId();

	public function deposit(Amount $amount, $accountId);

	public function fetchAccounts();
}
