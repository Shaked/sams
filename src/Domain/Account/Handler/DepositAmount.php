<?php
namespace Domain\Account\Handler;

use BigName\EventDispatcher\Dispatcher;
use BigName\EventDispatcher\Event;
use BigName\EventDispatcher\Listener;
use \Domain\Account\CouldNotDepositAccountException;
use \Domain\Account\Event\AccountDeposited;
use \Domain\Account\Repository;
use \Domain\HasEvents;

class DepositAmount implements Listener {
	use HasEvents;

	/**
	 * @var mixed
	 */
	private $dispatcher;

	/**
	 * @var mixed
	 */
	private $repository;

	/**
	 * @param Dispatcher $dispatcher
	 * @param Repository $repository
	 */
	public function __construct(Dispatcher $dispatcher, Repository $repository) {
		$this->dispatcher = $dispatcher;
		$this->repository = $repository;
	}

	/**
	 * @param Event $event
	 */
	public function handle(Event $event) {
		$status = $this
			->repository
			->deposit(
				$event->getAmount(),
				$event->getAccountId()
			)
		;

		if (!$status) {
			throw new CouldNotDepositAccountException($event->getAccountId());
		}

		$this->dispatcher->dispatch(new AccountDeposited($event->getAccountId()));
	}
}