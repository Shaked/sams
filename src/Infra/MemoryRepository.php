<?php
namespace Infra;
use \Domain\Account\Repository;

class MemoryRepository implements Repository {
	/**
	 * @var array
	 */
	private $accounts = [];

	/**
	 * @param $id
	 */
	public function create($id) {
		$this->accounts[$id] = new \Domain\Balance(0.0);
	}

	/**
	 * @param \Domain\Amount $amount
	 * @param $accountId
	 */
	public function deposit(\Domain\Amount $amount, $accountId) {
		$balance = $this->accounts[$accountId];
		$newBalance = $balance->deposit($amount);
		$this->accounts[$accountId] = $newBalance;
		return true;
	}

	/**
	 * @return mixed
	 */
	public function fetchAccounts() {
		return $this->accounts;
	}

	/**
	 * @return mixed
	 */
	public function generateId() {
		$ids = array_keys($this->accounts);
		$lastId = end($ids);
		if (!$lastId) {
			$lastId = 0;
		}
		return ($lastId + 1);
	}
}