<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');
require_once __DIR__ . '/../vendor/autoload.php';

use BigName\EventDispatcher\Dispatcher;
use Infra\MemoryRepository;
use Symfony\Component\HttpFoundation\Response;

$app = new \Silex\Application();

$app->post('/account/create/{amount}', function ($amount) {
	$dispatcher = new Dispatcher;
	$saga = new \Domain\Saga($dispatcher);
	$service = new \Application\AccountService(
		$dispatcher,
		new MemoryRepository()
	);
	$account = $service->execute($amount);
	return new Response(sprintf('New account %s created with balance: %s', $account->getId(), $account->getBalance()));
})->value('amount', 0.0);

$app->run();