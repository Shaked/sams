## Architecture

### Overview

SAMS follows [Domain Driven Design architecture](https://en.wikipedia.org/wiki/Domain-driven_design) and focuses on [Domain Events](https://lostechies.com/jimmybogard/2014/05/13/a-better-domain-events-pattern/) and [SAGA](https://msdn.microsoft.com/en-us/library/jj591569.aspx).

### How

There are 3 main flows to follow:

#### Create Account

![Create Account](docs/CreateAccount.png)

#### Deactivate Account

![Deactivate Account](docs/DeactivateAccount.png)

#### Handlers

![Handlers](docs/Handlers.png)


### Why

Basically, there few things to think about:

- Domains
- Contexts
- Aggregates and Entities
- Value Objects

At first, one might think that the only domain this software contains is the `Account` domain. This might be true if the application stays small mainly because of the upcoming overhead, which should be very important for a bigger software.

The possible domains are:
 - `Account`
 - `Customer` (although not mentioned in the assignment but helps to understand the relations).

Splitting the contexts might be tricky as well, there are several issues to consider:
- Consistency
- Failures
- Coupling

Therefore, the domains are not bounded to each other and instead they will communicate by using the `Domain Events` and maintained by a coordinator called `Saga`. This allows to achieve consistency and coupling while still leaves failures as a critical issue. Using the Saga it might be easier to compensate in case of failure during the transaction(s).

Note that the `Saga` doesn't do more than just coordinating the events.

#### Create Account

As mentioned above, the `AccountAggregate` communicates with its `Handlers` by using the `Saga`. In this case, the aggregate will generate a unique ID and pass it on to the Saga in order to adjust the account's balance. The Saga fires the relevant event in case of success or failure.

#### Deactivate Account

Deactivating the account doesn't require much as its almost the same idea as the way an account is being created but without any balance change.

#### Handlers

Although this is more or less straight-forward, it is important to understand that a `Transfer` is a combination of `Withdraw` from `Account` and `Deposit` to another. This will be maintained by the `Saga` which will fire those events respectively.

### Q&A

#### How should monetary values/decimals/fractions be handled?

Dealing with monetary values requires a very clear explanation about the need of precision. Handling it will of course require the value objects to be implemented accordingly. E.g:

What one should **not** do:

```
if ($amount == $balance) { .... }
```

But instead use:
```
if ($amount->equals($balance)) { ... }
```

#### How should we deal with transactions and history?

The entire idea of Domain Events is the ability to go back and learn about what happened until a specific point.

#### Architecture: what are the contexts, domains, entities, attributes?

##### Aggregates and Entities:
- AccountAggregate

##### Value Objects
- Balance
- Amount

Domains and Contexts are mentioned above.
